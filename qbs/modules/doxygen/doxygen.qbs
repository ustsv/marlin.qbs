import qbs
import qbs.Environment
import qbs.Probes
import qbs.Utilities
import qbs.ModUtils
import qbs.TextFile
import "utils.js" as utils

Module {
    name: doxygen
    version: '0.1.0'
    condition: true

    additionalProductTypes: ["qdoc-output", "doxygen_out", "doxygen_out2", "doxygen_tmp"]

    validate: {
        if (!doxygenProbe.found) {
            var str = "doxygenProbe not found " + doxygenProbe.names + " in "
                    + doxygenProbe.pathPrefixes
            console.error("doxygenProbe:: warning: " + str)
            return false
        }
        if (!graphvizProbe.found) {
            var str = "graphvizProbe not found " + graphvizProbe.names + " in "
                    + graphvizProbe.pathPrefixes
            console.error("graphvizProbe:: warning: " + str)
            return false
        }
        return true
    }

    property string qdocOutputDir: product.buildDirectory + "/html"

    Probes.BinaryProbe {
        id: doxygenProbe
        names: ["doxygen"]
        pathPrefixes: [
            "C:/Program Files (x86)",
            "C:/Program Files",
            "C:/usr/bin/",
            "C:/msys2/usr/bin",
            "C:/msys2/mingw32/bin",
            "C:/msys2/mingw64/bin"
        ]
    }

    Probes.BinaryProbe {
        id: graphvizProbe
        names: ["dot"]
        pathPrefixes: [
            "C:/Program Files (x86)/graphviz/bin",
            "C:/Program Files/graphviz/bin",
            "C:/usr/bin/Graphviz/bin",
            "C:/msys2/usr/bin",
            "C:/msys2/mingw32/bin",
            "C:/msys2/mingw64/bin"
        ]
    }

    FileTagger {
        patterns: ["Doxyfile", "doxyfile"]
        fileTags: ["doxygen_cfg"]
    }

    property string graphvizFilePath: graphvizProbe.filePath
    property string doxygenFilePath: doxygenProbe.filePath

    Rule {
        id: doxygenCfg
        condition: graphvizProbe.found
        alwaysRun: true
        multiplex: false
        inputs: ["doxygen_cfg"]

        outputArtifacts: [{
                filePath: Utilities.getHash(
                              input.baseDir) + "." + input.baseName,
                fileTags: ["doxygen_cfg_tmp"]
            }]

        outputFileTags: ["doxygen_cfg_tmp"]

        prepare: {
            console.info(ModUtils.moduleProperty(product, 'src'))
            var cmd = new JavaScriptCommand()
            cmd.workdir = product.buildDirectory
            cmd.outfile = product.buildDirectory + "\\" + Utilities.getHash(input.baseDir) + "." + input.baseName
            cmd.graphvizFilePath = ModUtils.moduleProperty(product, 'graphvizFilePath')
            cmd.description = "***\tpatching\t***\t" + input.filePath + "\t" + cmd.outfile
            cmd.highlight = "filegen"
            cmd.silent = false
            cmd.sourceCode = function () {
                var file = new TextFile(input.filePath, TextFile.ReadOnly)
                var content = file.readAll()
                file.close()
                var reg = new RegExp()
                content = content.replace(/^#(.*)$/gm, "")
                content = content.replace(/^[ \t]*\s+$/gm, "")
                content = content.replace(/^(.*)\\\s+/gm, "$1")
                content = content.replace(/^[ \t]*(OUTPUT_DIRECTORY)[ \t]*=.*$/gm, "$1 = " + workdir)
                content = content.replace(/^[ \t]*(DOT_PATH)[ \t]*=.*$/gm, "$1 = " + graphvizFilePath)
                content = content.replace(/^[ \t]*(INPUT.*)$/gm, "$1 " + "qwerty")
//                content = content.replace(/^[ \t]*CLANG_ASSISTED_PARSING[ \t]*=.*$/gm, "")
//                content = content.replace(/^[ \t]*CLANG_OPTIONS[ \t]*=.*$/gm, "")
//                content = content.replace(/^[ \t]*LATEX_TIMESTAMP[ \t]*=.*$/gm, "")
//                content = content.replace(/^[ \t]*WARN_AS_ERROR[ \t]*=.*$/gm, "")
                file = new TextFile(outfile, TextFile.WriteOnly)
                file.write(content)
                file.close()
            }

            return cmd
        }
    }

    Rule {
        id: doxygenGen
        condition: doxygenProbe.found && false
        alwaysRun: false
        multiplex: false

        inputs: ["doxygen_cfg_tmp"]

        outputArtifacts: [{
                filePath: "doxygen_sqlite3.db",
                fileTags: ["doxygen_tmp"]
            }, {
                filePath: ".\\html\\",
                fileTags: ["doxygen_out"]
            }]

        outputFileTags: ["doxygen_tmp", "doxygen_out"]

        prepare: {
            var args = [input.filePath]
            var doxygenFilePath = ModUtils.moduleProperty(product, 'doxygenFilePath')
            var cmd = new Command(doxygenFilePath, args)
//            cmd.sourceDir = product.sourceDirectory
            cmd.workdir = product.buildDirectory
            cmd.description = "***\tdoxygen\t***\t"
            cmd.highlight = "filegen"
            cmd.silent = false
            cmd.workingDirectory = product.sourceDirectory

            return cmd
        }
    }

    Rule {
        id: doxygenPost
        condition: true
        multiplex: false
        alwaysRun: false
        inputs: ["doxygen_out"]
        Artifact {
            fileTags: ["doxygen_out2"]
            filePath: ["doxygen.lst"]
        }

        prepare: {
            var args = []
            var cmd = new JavaScriptCommand()
            cmd.sourceCode = function () {
                var file = new TextFile(output.filePath, TextFile.WriteOnly)

                file.close()
            }
            cmd.description = "***\tDoxygen Post Rule"
            cmd.silent = false
            return cmd
        }
    }
}
