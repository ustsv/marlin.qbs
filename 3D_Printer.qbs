import qbs

Project {
    name: "Marlin_3D_Printer"
    qbsSearchPaths: [ "qbs", "pvs"]
    minimumQbsVersion: "1.11.0"

    references: [
        "src.qbs"
        //        "pvs/pvs.qbs"
    ]
}
