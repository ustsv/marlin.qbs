import qbs
import qbs.FileInfo
import qbs.ModUtils
import qbs.Utilities
import qbs.TextFile
import qbs.Environment
import qbs.Probes

Project {
    id: Marlin
    name: "Marlin"
    minimumQbsVersion: "1.11.0"

    Product {
        id: config
        name: "config"
        type: [ "info "]

        Depends {
            name: "git"
            required: true
            Repo: product.sourceDirectory + "\\..\\Marlin"
        }

        Properties {
            condition: true
            APP_VERSION: { console.info("git.Version:\t" + git.Version) }
            APP_VERSION2: { console.info("git.Repo:\t" + git.Repo) }
            APP_VERSION3: { console.info("GitProbe.filePath:\t" + git.GitPath) }
        }

        Probes.BinaryProbe {
            id: arduinoPath
            platformPaths: {    }
            pathPrefixes: {
                var paths = base
                var env32 = Environment.getEnv("PROGRAMFILES")
                var env64 = Environment.getEnv("ProgramW6432")
                var msys2 = Environment.getEnv("MSYS2ROOT")
                var scoop = Environment.getEnv("SCOOP")
                var scoop_global = Environment.getEnv("SCOOP_GLOBAL")
                paths.push(FileInfo.joinPaths(env32, "/arduino"))
                paths.push(FileInfo.joinPaths(env64, "/arduino"))
                paths.push(FileInfo.joinPaths(scoop, "/apps/arduino/current"))
                paths.push(FileInfo.joinPaths(scoop_global, "/apps/arduino/current"))
                paths.push(FileInfo.joinPaths(msys2, "usr/bin"))
                paths.push(FileInfo.joinPaths(msys2, "mingw32/bin"))
                paths.push(FileInfo.joinPaths(msys2, "mingw64/bin"))
                return paths
            }
            names: ["arduino"]
        }

        readonly property string arduino: {
            if (!arduinoPath.found) {
                throw arduinoPath.found
            }
            return arduinoPath.path
        }

        readonly property string marlinSrc: "..\\Marlin\\Marlin\\"

        PropertyOptions {
            name: "arduino"
            allowedValues: []
            description: "path for arduino dir"
        }

        FileTagger { fileTags: ["subst"]
            patterns: ["*.in"]
        }

        Export {
            Depends {
                name: "cpp"
            }
            Depends {
                name: "pvs"
            }

            pvs.analize: false

            readonly property string arduino: product.arduino
            readonly property string marlinSrc: product.marlinSrc

            cpp.defines: [
                "ARDUINO_AVR_MEGA2560",
                "ARDUINO_ARCH_AVR",
                "F_CPU=16000000L",
                "ARDUINO=100",
                "__AVR_ATmega2560__"]

            cpp.useCPrecompiledHeader: false
            cpp.useCxxPrecompiledHeader: false

            cpp.staticLibrarySuffix: ".a"
            cpp.positionIndependentCode: false
            cpp.executableSuffix: ".elf"
            cpp.executablePrefix: ""
            cpp.separateDebugInformation: true
            cpp.debugInfoSuffix: ".debug"

            cpp.archiverName: "gcc-ar"

            Properties {
                condition: qbs.buildVariant == "release"
                cpp.debugInformation: false
                cpp.defines: outer.concat("NDEBUG")
                cpp.optimization: "small"
            }

            Properties {
                condition: qbs.buildVariant == "debug"
                cpp.debugInformation: true
                cpp.defines: outer.concat("DEBUG")
                cpp.optimization: "none"
            }

            cpp.includePaths: [
                FileInfo.joinPaths(arduino, "hardware/arduino/avr/arduino"),
                FileInfo.joinPaths(arduino, "hardware/arduino/avr/variants/mega"),
                FileInfo.joinPaths(arduino, "hardware/arduino/avr/cores/arduino")
            ]

            cpp.linkerMode: "automatic"

            Properties {
                condition: true
                cpp.commonCompilerFlags: outer.concat(
                                             "-c",
                                             "-ffunction-sections",
                                             "-fdata-sections",
                                             "-MMD",
                                             "-flto",
                                             "-Wno-expansion-to-defined",
                                             "-Wno-unused" )
                cpp.linkerFlags: outer.concat(
                                     "--gc-sections",
                                     "--relax")
                cpp.driverFlags: outer.concat(
                                     "-mmcu=atmega2560",
                                     "-fuse-linker-plugin")
                cpp.driverLinkerFlags: outer.concat(
                                           )
                cpp.cxxFlags: outer.concat(
                                  "-std=gnu++11",
                                  "-fpermissive",
                                  "-fno-exceptions",
                                  "-fno-threadsafe-statics")
                cpp.cFlags: outer.concat(
                                "-std=gnu11",
                                "-fno-fat-lto-objects")
                cpp.includePaths: outer.concat(
                                      FileInfo.joinPaths(arduino,"libraries/LiquidCrystal/src"))
            }
        }
    }

    MyApplication {
        id: marlin
        name: "marlin"
        version: "0.1.0"
        builtByDefault: true
        targetName: "marlin"
        //        destinationDirectory: "marlin"
        Depends {
            name: "config"
        }
        Depends {
            name: "core"
        }
        Depends {
            name: "LiquidCrystal"
        }
        type: ["application", "map", "hex", "eeprom", "info"]
        consoleApplication: true
        pvs.analize: false

        Properties {
            condition: true
            cpp.linkerFlags: outer.concat("-Map=" + FileInfo.joinPaths(destinationDirectory, product.targetName + ".map"))
        }

        Group {
            name: "Src"
            overrideTags: false
            fileTags: ["src"]
            prefix: config.marlinSrc
            files: [
                "**/*.cpp",
                "**/*.c"
            ]
        }

        Group {
            name: "Inc"
            overrideTags: false
            //            fileTags: ["src"]
            prefix: config.marlinSrc
            files: [
                "**/*.h",
                "**/*.hpp"
            ]
            excludeFiles: [
                "Configuration.h",
                "c_pch_src.hpp",
                "cpp_pch_src.hpp"
            ]
        }

        Group {
            name: "Ino"
            overrideTags: false
            //            fileTags: ["src"]
            prefix: config.marlinSrc
            files: [
                "*.ino",
                "Configuration.h"
            ]
        }

        Group {
            name: "Asm"
            overrideTags: false
            fileTags: ["src"]
            prefix: config.marlinSrc
            files: [
                "**/*.S",
                "**/*.s"
            ]
        }

        Group {
            name: "PrecompiledHeader"
            files: [
                "c_pch_src.hpp",
                "cpp_pch_src.hpp"
            ]
        }

        Group {
            name: "Install"
            fileTagsFilter: ["application", "hex", "eeprom", "map"]
            qbs.install: true
            qbs.installDir: "."
        }

        FileTagger {
            fileTags: ["c_pch_src"]
            patterns: "c_pch_src.hpp"
        }

        FileTagger {
            fileTags: ["cpp_pch_src"]
            patterns: "cpp_pch_src.hpp"
        }

        FileTagger {
            id: map
            fileTags: ["map"]
            patterns: "*.map"
        }
    }

    StaticLibrary {
        id: core
        name: "core"
        Depends {
            name: "config"
        }
        type: ["staticlibrary"]
        targetName: "core"
        //        destinationDirectory: "core"
        consoleApplication: true

        Group {
            name: "Core"
            overrideTags: false
            fileTags: ["src"]
            prefix: FileInfo.joinPaths(config.arduino,"hardware/arduino/avr/cores/arduino/")
            files: [
                "**/*.cpp",
                "**/*.c",
                "**/*.S",
                "**/*.s"
            ]
        }
    }

    StaticLibrary {
        id: LiquidCrystal
        name: "LiquidCrystal"
        Depends {
            name: "config"
        }

        type: ["staticlibrary"]
        targetName: "LiquidCrystal"
        consoleApplication: true

        Group {
            name: "LiquidCrystal"
            overrideTags: false
            fileTags: ["src"]
            prefix: FileInfo.joinPaths(config.arduino,"libraries/LiquidCrystal/src/")
            files: [
                "**/*.cpp",
                "**/*.c"
            ]
        }
    }

    /*
    Product { name: "pvs_analize"
        builtByDefault: false
        type: ["pvs_out"]
        Depends { name: "pvs" }
        Depends { name: "LiquidCrystal"
            required: true }
        Depends { name: "core"
            required: true }
        Depends { name: "marlin"
            required: true }

        pvs.analize: true

        Group { name: "pvs"
            condition: config.analize
            overrideTags: true
            fileTagsFilter: ["src"]
            fileTags: ["pvs_in"]
            qbs.install: true
            qbs.installDir: "pvs"
        }
    }
    */
}
