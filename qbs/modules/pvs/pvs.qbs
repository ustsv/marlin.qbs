import qbs
import qbs.Probes
import qbs.TextFile
import qbs.Utilities
import qbs.ModUtils
import qbs.Process
import qbs.File
import qbs.FileInfo
import qbs.Environment
import "pvs_utils.js" as utils

Module {
    name: "pvs"

    additionalProductTypes: [ "pvs_out" ]

    property bool analize: false

    Probes.BinaryProbe {
        id: PVS_StudioProbe

        platformPaths: {
        }

        pathPrefixes: {
            var paths = base;
            var env32 = Environment.getEnv("PROGRAMFILES");
            var env64 = Environment.getEnv("ProgramW6432");
            var msys2 = Environment.getEnv("MSYS2");
            paths.push(FileInfo.joinPaths(env32, "/PVS-Studio/x86"));
            paths.push(FileInfo.joinPaths(env64, "/PVS-Studio/x64"));
            paths.push("C:/usr/bin/PVS-Studio/x64");
            paths.push(FileInfo.joinPaths(msys2, "usr/bin"));
            paths.push(FileInfo.joinPaths(msys2, "mingw32/bin"));
            paths.push(FileInfo.joinPaths(msys2, "mingw64/bin"));
            return paths;
        }
        names: ["PVS-Studio"]
    }

    validate: {
        return true;
    }

    property string PVS_StudioPath: {
        if (PVS_StudioProbe.found)
            return PVS_StudioProbe.filePath;
        var str = "PVS_StudioProbe not found " + PVS_StudioProbe.names + " in " + PVS_StudioProbe.pathPrefixes;
        console.error("error\n" + str);
        //                throw str;
        return "undefined";
    }

    FileTagger { fileTags: ["pvs_cfg"];
        patterns: ["PVS-Studio.cfg"];
    }

    FileTagger { fileTags: ["pvs_out"];
        patterns: [".plog"];
    }

    Rule {
        id: PVS_Cfg
        condition: parent.analize
        multiplex: true
        alwaysRun:  false
        requiresInputs: false
        inputs: [ ]
        Artifact {
            fileTags:   ["pvs_cfg"]
            filePath:   ["PVS-Studio.cfg"]
        }

        prepare: {
            var cmd = new JavaScriptCommand();
            cmd.description = "***\tPVS_StudioCfg\t***";
            cmd.silent = false;
            cmd.sourceCode = function() {
                var appdata = Environment.getEnv("APPDATA");
                var file = new TextFile(output.filePath, TextFile.WriteOnly);
                file.writeLine("platform=win32");
                file.writeLine("lic-file=" + appdata + "/PVS-Studio/PVS-Studio.lic");
                file.writeLine("preprocessor=gcc");
                file.writeLine("language=C++");
                file.writeLine("analysis-mode=0");
                file.writeLine("exclude-path=C:/QT/");
                file.writeLine("sourcetree-root=" + product.sourceDirectory);
                file.writeLine("errors-off=V112 V122 V107 V2006 V004 V550");
                file.close();
            }
            return cmd;
        }
    }

    Rule {
        id: PVS_Plog
        condition: product.pvs.analize
        multiplex: false
        alwaysRun:  false
        inputs: [ "src" ]
        //        inputsFromDependencies: [ "src" ]
        auxiliaryInputs:  [ "pvs_cfg" ]

        Artifact {
            fileTags: [ "pvs_out" ]
            filePath: [ Utilities.getHash(input.baseDir) + "/" + input.fileName + ".plog" ] //
            //            filePath: [ FileInfo.joinPaths(product.buildDirectory, "PVS-Studio/")
            //                + input.fileName + ".plog" ] // Utilities.getHash(input.baseDir) +
        }

        prepare: {
            var defines = product.cpp.defines;
            var includePaths = product.cpp.includePaths;
            var commonCompilerFlags = product.cpp.commonCompilerFlags;
            var dir = product.buildDirectory;
            //            File.makePath(FileInfo.joinPaths(dir, "PVS-Studio"));
            var PVS_StudioPath = ModUtils.moduleProperty(product, "PVS_StudioPath");
            var arg = ["--cfg", dir + "/PVS-Studio.cfg",
                       "--source-file", input.filePath,
//                       "--output-file", output.filePath,
                       "--cl-params", input.filePath,
                       "-DPVS_STUDIO"];

            for (val in defines) {
                arg.push("-D" + defines[val]);
            }

            for (val in includePaths) {
                arg.push("-I" + includePaths[val]);
            }
            arg.push("-IC:/usr/bin/arduino/hardware/tools/avr/avr/include");

            for (val in commonCompilerFlags) {
                arg.push(commonCompilerFlags[val]);
            }

            var cmd = new Command();
            cmd.description = "***\tPVS_Plog\t***";
            //            cmd.extendedDescription = "PVS_Studio parser file rule."
            cmd.silent = false;
            cmd.highlight = "compiler";
            cmd.responseFileUsagePrefix = "@";
            cmd.workingDirectory = product.buildDirectory;
            cmd.program = PVS_StudioPath;
            cmd.arguments = arg;

            cmd.stderrFilePath = cmd.stdoutFilePath;
            cmd.stdoutFilterFunction = function(output) {
                return "test_output" + output;
            }

            cmd.stderrFilterFunction = function(output) {
                return;
            }

            return cmd;
        }
    }

    Rule {
        id: PVS_log
        condition: parent.analize
        multiplex: true
        alwaysRun:  true
        requiresInputs: true
        inputs: [ "pvs_out" ]
        Artifact {
            fileTags:   ["pvs_log"]
            filePath:   ["PVS-Studio.log"]
        }

        prepare: {
            var cmd = new JavaScriptCommand();
            cmd.description = "***\tPVS_StudioLog\t***";
            cmd.silent = false;
            cmd.highlight = "compiler"
            cmd.sourceCode = function() {
                var fo = new TextFile(output.filePath, TextFile.WriteOnly);
                var objs = inputs["pvs_out"]

                for (f in objs) {
                    //                    console.info(objs[f].filePath);
                    fi = new TextFile(objs[f].filePath, TextFile.ReadOnly);
                    content = fi.readAll();
                    str = content.split("\n");
                    for (n in str) {
                        arg = str[n].split("<#~>");
                        if (arg.length == 14) {
                            w = arg[3] + "\t" + arg[2] + ":warning:" + arg[5] + "\t" + arg[6] + "";
                            console.info(w);
                            console.error(readStdErr());
                        }
                    }
                    console.error("Test error");
                    fi.close();
                }
                fo.close();
                //                var fi = new TextFile("Z:/tmp/build-MCVD-Desktop_Qt_5_10_1_MinGW_32bit-Debug/pvs.log",TextFile.ReadOnly)
                //                while( !fi.eof) {
                //                    c = fi.readLine()
                //                    console.error(c)
                //                }
                //                fi.close()
            }
            return cmd;
        }
    }
}
