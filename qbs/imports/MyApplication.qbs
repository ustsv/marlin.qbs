import qbs
import qbs.FileInfo
import qbs.Probes
import qbs.Environment

Product {
    Depends { name: "cpp" }

    Rule {
        id: git
        condition: true
        multiplex: true
        alwaysRun: true
        inputs: ["git"]
        Artifact {
            fileTags: ["info"]
            filePath: FileInfo.joinPaths(qbs.nullDevice, "info")
        }
        prepare: {
            var args = ["describe", "--tags", "--long"];
            var cmd = new Command("git", args);
            cmd.description = "git" + input.filePath;
            cmd.silent = false;
            cmd.highlight = "compiler";
            return [cmd];
        }
    }

    Rule {
        id: map
        condition: true
        multiplex: false
        inputs: ["application"]
        Artifact {
            fileTags: ["map"]
            filePath: product.targetName + ".map"
        }
        prepare: {
            var args = []
            var cmd = new JavaScriptCommand()
            cmd.sourceCode = function () {}
            cmd.description = "fake map Rule"
            cmd.silent = false
            return [cmd]
        }
    }

    Rule {
        id: size
        condition: true
        multiplex: false
        alwaysRun: true
        inputs: ["application"]
        Artifact {
            fileTags: ["info"]
            filePath: FileInfo.joinPaths(qbs.nullDevice, "info")
        }
        prepare: {
            var args = [input.filePath];
            var cmd = new Command(FileInfo.joinPaths(product.cpp.toolchainInstallPath, product.cpp.toolchainPrefix + "size"), args);
            cmd.description = "size " + input.filePath;
            cmd.silent = false;
            cmd.highlight = "compiler";
            return [cmd];
        }
    }

    Rule {
        id: hex
        inputs: ["application"]
        Artifact {
            fileTags: ["hex"]
            filePath: product.targetName + ".hex"
        }
        prepare: {
            var args = ["-O", "ihex", "-R .eeprom", input.filePath, output.filePath];
            var cmd = new Command(product.cpp.objcopyPath, args);
            cmd.description = "converting '" + input.filePath + "' to hex";
            cmd.silent = false;
            cmd.highlight = "compiler";
            return [cmd];
        }
    }

    Rule {
        id: eep
        inputs: ["application"]
        Artifact {
            fileTags: ["eeprom"]
            filePath: product.targetName + ".eep"
        }
        prepare: {
            var args = ["-O", "ihex", "-j .eeprom --set-section-flags=.eeprom=alloc,load --no-change-warnings --change-section-lma .eeprom=0", input.filePath, output.filePath]
            var cmd = new Command(product.cpp.objcopyPath, args)
            cmd.description = "converting '" + input.filePath + "' to eeprom"
            cmd.silent = false
            cmd.highlight = "compiler"
            return [cmd]
        }
    }
}
