import qbs
import qbs.Probes
//import qbs.TextFile
//import qbs.Utilities
//import qbs.ModUtils
import qbs.Process
//import qbs.File
import qbs.FileInfo
import qbs.Environment

Module {
    name: "git"
    additionalProductTypes: [ "info" ]

    Probes.BinaryProbe {
        id: GitProbe
        platformPaths: { }
        pathPrefixes: {
            var paths = base;
            var env32 = Environment.getEnv("PROGRAMFILES");
            var env64 = Environment.getEnv("ProgramW6432");
            var msys2 = Environment.getEnv("MSYS2");
            paths.push(FileInfo.joinPaths(env32, "/git/x86"));
            paths.push(FileInfo.joinPaths(env64, "/git/x64"));
            paths.push(FileInfo.joinPaths(msys2, "usr/bin"));
            paths.push(FileInfo.joinPaths(msys2, "mingw32/bin"));
            paths.push(FileInfo.joinPaths(msys2, "mingw64/bin"));
            return paths;
        }
        names: ["git"]
    }

    readonly property string GitPath: {
        if (!GitProbe.found) {
            throw GitProbe.found;
        }
        return GitProbe.filePath;
    }

    property string Repo: ""
    Parameter {
        property string Repo: {
            return "test"
        }
    }

    PropertyOptions { name: "Repo"
        allowedValues: "1.2.3"
        description: "path for git repo"
    }

    readonly property string Version: ""
    Parameter {
        property string Version
    }
    PropertyOptions { name: "Version"
        allowedValues: ""
        description: "return Version from git repo"
    }

//    Probe {
//        id: gitVersion;
//        property string value;
//        configure: {
//            var p = new Process();
//            var out;
//            console.info("gitVersion");
//            console.info("gitVersion:Repo\t");
//            console.info("gitVersion:GitPath\t" + product.GitPath);
//            var arg = [ "describe", "--tags", "--long" ];
//            //        p.setEnv("LANG","ru_RU.CP1251");
//            p.setCodec("ru_RU.CP1251");
//            p.setWorkingDirectory(product.Repo);
//            if( p.exec(GitPath, arg)===0 ) {
//                var out = p.readStdOut().split("-").join("/");
//                console.info(out);
//            }
//            p.close();
//            value = out;
//            found = true;
//        }
//    }

    //    Rule {
    //        id: git
    //        condition: true
    //        multiplex: true
    //        alwaysRun: true
    //        requiresInputs: false
    //        inputs: ["git"]
    //        Artifact {
    //            fileTags: ["info"]
    //            filePath: FileInfo.joinPaths(qbs.nullDevice, "info")
    //        }
    //        prepare: {
    //            //            console.info("product.git.gitRepo:\t" + product.git.gitRepo);
    //            var cmd = new Command();
    //            cmd.program = product.git.GitPath;
    //            cmd.environment.push("LANG=ru_RU.CP1251");
    //            cmd.arguments = ["describe", "--tags", "--long"];
    //            cmd.workingDirectory = product.git.gitRepo;
    //            //            cmd.stderrFilePath = "c:\\tmp\\eer.log"
    //            //            cmd.stdoutFilePath = "c:\\tmp\\out.log"
    //            cmd.input = input;
    //            cmd.output = output;
    //            cmd.inputs = inputs;
    //            cmd.outputs = outputs;
    //            cmd.stdoutFilterFunction = function(out) {
    //                //                for(var prop in inputs.git) {
    //                //                    console.info("prop:\t" + prop + " =\t" + inputs.git[prop].filePath)
    //                //                }
    //                GIT_VERSION = out.split("-").join("/");
    //                return out.split("-").join("/");
    //            };
    //            //cmd.responseFileUsagePrefix
    //            //cmd.responseFileArgumentIndex
    //            cmd.description = "git";
    //            cmd.silent = false;
    //            cmd.highlight = "linker";
    //            return [cmd];
    //        }
    //    }
}
