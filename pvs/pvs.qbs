import qbs
import qbs.Probes
import qbs.TextFile
import qbs.Utilities
import qbs.ModUtils

Module {
    name: "pvs"

    builtByDefault: true

    type: [ "pvs_cfg", "pvs_out" ]

    Group { name: "pvs_cfg"
        files: [ "PVS-Studio.cfg" ]
    }

    Probes.BinaryProbe {
        id: PVS_StudioProbe
        pathPrefixes: [
            "C:/Program Files (x86)/PVS-Studio/x64",
            "C:/Program Files/PVS-Studio/x64",
            "C:/usr/bin/PVS-Studio/x64",
            "C:/msys2/usr/bin",
            "C:/msys2/mingw32/bin",
            "C:/msys2/mingw64/bin"
        ]
        names: ["PVS-Studio"]
    }

//    validate: {
//        return true;
//    }

    property string PVS_StudioPath: {
        if (PVS_StudioProbe.found)
            return PVS_StudioProbe.filePath;
        var str = "PVS_StudioProbe not found " + PVS_StudioProbe.names + " in " + PVS_StudioProbe.pathPrefixes;
        console.error("error\n" + str);
        //        throw str;
        return "undefined";
    }

    FileTagger { fileTags: ["pvs_cfg"];
        patterns: ["PVS-Studio.cfg"];
    }

    Rule {
        id: PVS_StudioCfg
        condition: true
        multiplex: true
        alwaysRun:  true
        inputs: [ ]
        Artifact {
            fileTags:   ["pvs_cfg"]
            filePath:   ["PVS-Studio.cfg"]
        }

        prepare: {
            var args = [];
            var cmd = new JavaScriptCommand();
            cmd.description = "***\tPVS_StudioCfg";
            cmd.silent = false;
            cmd.sourceCode = function() {
//                console.info("***\tPVS_StudioCfg");
                var file = new TextFile(output.filePath, TextFile.WriteOnly);
                //                pvs_studio.target = pvs
                //                pvs_studio.output = true
                //                #pvs_studio.format =
                //                pvs_studio.sources = $${SOURCES}
                //                pvs_studio.cfg = PVS-Studio.cfg
                file.writeLine("platform=win32");
                file.writeLine("language=C++");
                //                file.writeln("lic-file=$$clean_path($$getenv(HOME)/AppData/Roaming/PVS-Studio/PVS-Studio.lic)");
                file.writeLine("preprocessor=gcc");
                file.writeLine("analysis-mode=4");
                //                #pvs_studio.cfg_text += "exclude-path=$$clean_path($$(QTDIR))/");
                var qt_patch
                file.writeLine("exclude-path=C:/QT/");
                //                file.writeln("exclude-path=./$${MOC_DIR}/");
                //                file.writeln( "exclude-path=./$${UI_DIR}/");
                file.
                writeLine("errors-off=V112 V107");
                var sourceDir = product.sourceDirectory;
                file.writeLine("sourcetree-root="+sourceDir+"/../");
                file.close();
            }
            return cmd;
        }
    }

    Rule {
        id: PVS_StudioPlog
        condition: true
        multiplex: false
        alwaysRun:  true
        inputs: [ "pvs_in" ]
        Artifact {
            fileTags: [ "pvs_out" ]
            filePath:  [input.fileName + ".plog" ]
//            filePath:  input.baseName + ".plog"
        }

        prepare: {
            var PVS_StudioProbe = ModUtils.moduleProperty(product, 'PVS_StudioProbe.filePath');
            console.info("---"+PVS_StudioProbe+"---");
            var args = [ input.filePath, '>', output.filePath];
            args.push('>');
            console.info(args);
//            var cmd = new Command(PVS_StudioProbe, args);
            var cmd=new Command("cat", args);
            cmd.description = "***\tPVS_StudioPlog";
            cmd.extendedDescription = "A command is what Qbs executes at build time. It is represented in the language by an object of type Command, which runs a process, or JavaScriptCommand, which executes arbitrary JavaScript code. A command is always created in the prepare script of a Rule. ";
            cmd.silent = false;
            cmd.highlight = "compiler";
            cmd.responseFileUsagePrefix = '@';
            return cmd;
        }
    }
}
