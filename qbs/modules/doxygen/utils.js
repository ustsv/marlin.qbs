//var File = loadExtension("qbs.File");
//var FileInfo = loadExtension("qbs.FileInfo");
var ModUtils = require("qbs.ModUtils");

//var PathTools = loadExtension("qbs.PathTools");
//var Process = loadExtension("qbs.Process");
//var UnixUtils = loadExtension("qbs.UnixUtils");
//var Utilities = loadExtension("qbs.Utilities");
//var WindowsUtils = loadExtension("qbs.WindowsUtils");

function dump(arr,level) {
    var dumped_text = ""
    if(!level) level = 0
    //The padding given at the beginning of the line.
    var level_padding = "";
    for(var j=0;j<level+1;j++) level_padding += ".   ";

    if(typeof(arr) == 'object' || typeof(arr) == 'function') { //Array/Hashes/Objects
        var i=0
        for(val in arr ) {
            if(typeof(arr[val]) == 'object') { //If it is an array,
                dumped_text += level_padding + "'" + i + "' ==> "
                dumped_text += val + " {\n" + dump(arr[val], level+1) + level_padding + "}\n";
            } else {
                dumped_text += level_padding + "'" + i + "' ==> \"" + val + "\"("+typeof(arr[val])+ ")\n";
            }
            i+=1;
        }
    } else { //Stings/Chars/Numbers etc.
        dumped_text += "===>"+arr+"<===("+typeof(arr)+")\n";
    }
    return dumped_text;
}



function dump_prop(start, maxlevel, level) {
    var i = 0
    var dumped_text = ""
    dumped_text += Object.valueOf(start) + "[" + typeof(start) + "]\n"
    for (prop in start) {
        dumped_text += start[prop] +" : " + prop + "\n"
    }

    return dumped_text

    if (!level) level = 0
    if (!maxlevel)  maxlevel = 0
    if (maxlevel === -1)
        return ""
    var i = 0
    var dumped_text = ""
    var pref = ""
    for (x = 0; x < level; x++)
        pref += (".   ")
    dumped_text += pref
    dumped_text += "[" + typeof start+ "] =\t"+start
    for (prop in start) {
        if (start.hasOwnProperty(prop)) {
            dumped_text += pref + level + "." + i + ":  "
            dumped_text += prop + " [" + typeof start[prop] + "] =\t\n"
            switch (typeof start[prop]) {
            case "string":
                dumped_text += start[prop] + "\n"
                break
            case "object":
                dumped_text += start + "\n"
                dumped_text += dump_prop(start[prop], maxlevel - 1, level + 1)
                break
            case "function":
                dumped_text += /*pref +*/ "[code]" + /*start[prop] +*/ "\n"
                break
            default:
                dumped_text += /*pref + */ start[prop] + "\n"
            }
            i++
        }
    }
    return dumped_text
}

var _qdocDefaultFileTag = "qdoc-output"

function qdocArgs(product, input, outputDir) {
    var args = [input.filePath]
    var qtVersion = ModUtils.moduleProperty(product, "versionMajor")
    if (qtVersion >= 5) {
        args.push("-outputdir")
        args.push(outputDir)
    }

    return args
}

function qdocFileTaggers() {
    var t = _qdocDefaultFileTag
    console.info("qdocFileTaggers")
    return {
        qhp: [t, "qhp"],
        sha1: [t, "qhp-sha1"],
        css: [t, "qdoc-css"],
        html: [t, "qdoc-html"],
        index: [t, "qdoc-index"],
        png: [t, "qdoc-png"]
    }
}

function outputArtifacts(product, input) {
    console.info("outputArtifacts")
    var tracker = new ModUtils.BlackboxOutputArtifactTracker()
    tracker.hostOS = product.moduleProperty("qbs", "hostOS")
    tracker.shellPath = product.moduleProperty("qbs", "shellPath")
    tracker.defaultFileTags = [_qdocDefaultFileTag]
    tracker.fileTaggers = qdocFileTaggers()
    tracker.command = "doxygen"
    tracker.commandArgsFunction = function (outputDirectory) {
        return [input.filePath]
    }
    tracker.commandEnvironmentFunction = function (outputDirectory) {
        return ""
    }
    //    ModUtils.dumpObject(tracker);
    return tracker.artifacts(ModUtils.moduleProperty(product, "qdocOutputDir"))
}
